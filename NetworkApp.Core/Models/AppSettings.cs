﻿namespace NetworkApp.Core.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
