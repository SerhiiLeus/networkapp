﻿using NetworkApp.Core.Models;
using System.Collections.Generic;

namespace NetworkApp.Core.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(int id);
    }
}
